import React,{useState} from 'react';
import { StyleSheet, Text, View,Dimensions } from 'react-native';
import { Text as Title,ListItem,Icon } from 'react-native-elements'
import LimitsOverlay from '../Components/LimitsOverlay'
import Circle from '../Components/Circle'
import UserTitle from '../Components/UserTitle'
interface participant{
    name:string,
    unit:number
  }
  interface mainCategory{
    name:string,
    // IconName:string,
    grade:any,
    color?:string
  }
export interface Props{
    userDetails:participant,
}
let {height,width}=Dimensions.get("screen")
let lastExam:{[id:number]:{name:string,grade:number|string}}={1:{name:"דופק לב",grade:60},2:{name:"קצב",grade:50},3:{name:"זמן",grade:"00:10:04"}}
let lastExamIcon:{[id:number]:{type:string,name:string,units:string,color:string}}={2:{type:"feather",name:"activity",color:"green",units:"Hz"},1:{type:"font-awesome",name:"heartbeat",color:"#f50",units:"Hz"},3:{type:"material-community",name:"alarm",color:"black",units:"sec"}}
let mainCategories:{[id:number]:mainCategory}={2:{name:"נפח שבועי מלא",grade:88},3:{name:"ממוצע ריצה שבועי",grade:"10:20:20"},4:{name:"ממוצע שעות שינה",grade:5}}

// var mainCategories:{[id:number]:mainCategory}={1:{name:"אחוז ביצוע שבוע מלא",IconName:"battery-",grade:80}, 
// 2:{name:"נפח שבועי מלא",IconName:"battery-",grade:88},3:{name:"ממוצע ריצה שבועי",IconName:"alarm",grade:"10:20:20",color:"black"},4:{name:"ממוצע שעות שינה",IconName:"bell",grade:5,color:"grey"}}
let th:{[id:number]:{min?:number,mid?:number,max?:number,type:string,IconName:string,color?:string}}={2:{min:65,mid:75,max:85,type:"material-community",IconName:"battery-"},3:{type:'material-community',IconName:"alarm",color:"black"},4:{type:'font-awesome-5',IconName:"bell",color:"grey"}}

var limits:{[id:number]:{name:string,isCheck:boolean}}={1:{name:"הליכה",isCheck:true},2:{name:"ריצה",isCheck:false},3:{name:"כוח",isCheck:true}}
function BatteryStatus(dic:{[id:number]:mainCategory},th:{[id:string]:{min?:number,mid?:number,max?:number,type:string,IconName:string,color?:string}}){
  Object.keys(th).map((key,index)=>{
    var keyInt=parseInt(key)
    if (th[keyInt]['IconName']==="battery-"){
          var NameExt=th[keyInt]['IconName']
            if (NameExt){
              th[keyInt]['IconName']=NameExt.concat(String(10*Math.round(dic[keyInt]['grade']/10)));
        }
      } 
    var grade=dic[keyInt]['grade']
    var max=th[keyInt]['max']
    var min=th[keyInt]['min']
    var mid=th[keyInt]['mid']

    if (min){
      if (max && grade>=max){
        th[keyInt]['color']='green'
      }
      else if(mid && grade>=mid) {
            th[keyInt]['color']='orange'
            }
            else{
              th[keyInt]['color']='red'
            }
    }


  })

}
BatteryStatus(mainCategories,th)
const UserScreen:React.FC=(props:any)=>  {
    const [visible, setVisible] = useState(false);
    const unitName:number=props.navigation.getParam('platoon')
    const name:string=props.navigation.getParam('name')
    return(
         <View style={styles.container}>
            <LimitsOverlay visible={visible} setVisible={setVisible} limits={limits} />
            
            <UserTitle unitName={unitName} name={name}/>
            {/* <UserTitle unitName={props.userDetails.unit} name={props.userDetails.name}/> */}
              
            <Title h4 h4Style={{...styles.subTitle,flexDirection:"row",justifyContent:"flex-end",width:"100%",marginTop:20,marginLeft:20}}>ביצועים שבועיים</Title>
            <View>
                {Object.keys(mainCategories).map((key1, index)=>{
                  var key=parseInt(key1)
                  return(
                    <ListItem key={key} titleStyle={{fontSize:20}} title={ <Title h4 h4Style={{fontSize:20}}>{mainCategories[key]['name']}</Title>} 
                      style={styles.participantContainer} onPress={()=>{}}
                      rightElement={
                        <Text style={{color:th[key]['color'],fontSize:20}} >{mainCategories[key]['grade']}</Text>} 
                      leftIcon={<Icon name={th[key]['IconName']} type={th[key]['type']} size={30}
                      color={th[key]['color']}/>}
                    />)} 
                )}
                <ListItem  leftIcon={<Icon name={"healing"} type={"material"} size={30} color={"#ff4500"} />}
                  title={<View><Title h4 h4Style={{fontSize:20}}>הגבלות</Title></View>} 
                  rightIcon={<Icon
                    name={"arrow-left"} type={"font-awesome"} size={25} color={"grey"} />}
                    onPress={()=>setVisible(true)}/>
            </View >  
            <Title h4 h4Style={{...styles.subTitle,flexDirection:"row",justifyContent:"flex-end",width:"100%",marginTop:20,marginLeft:20}}>בוחן אחרון</Title>
            <View style={{flexDirection:"row",flexWrap:"wrap",justifyContent:"space-between",marginHorizontal:20,marginVertical:10}}>
            {Object.keys(lastExam).map((key1,index)=>{
              var key=parseInt(key1)
              return(
                  <View key={key}>
                    <Circle name={lastExamIcon[key]['name']}
                      units={lastExamIcon[key]['units']}
                      IconType={lastExamIcon[key]['type']}
                      color={lastExamIcon[key]['color']}
                      grade= {lastExam[key]['grade']}/>
                  </View>
               )})}
          </View>


         </View>
     )

 }
 export default UserScreen

 const styles = StyleSheet.create({
    container: {
      // paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
      flex: 1,
      marginHorizontal:12
    },
    subTitle:{
      alignSelf:"center",
      fontWeight:"normal",
      fontSize:20,
      paddingBottom:5
    },
    participantContainer:{
      borderBottomColor: "rgba(220,220,220,0.5)",
      borderBottomWidth: 1,
       flexDirection:"row",
       alignItems:"center" ,
  },
})
  // const [check1, setCheck1] = useState(limits[1]["isCheck"]);
  // const [check2, setCheck2] = useState(limits[2]["isCheck"]);
  // const [check3, setCheck3] = useState(limits[3]["isCheck"]);
// function initCheck(){
//   Object.keys(limits).map((key,index)=>{
//     eval("setCheck"+key)(limits[key]["isCheck"])
//   })
// }
// function onAcceptLimits(){
//   Object.keys(limits).map((key,index)=>{
//     limits[key]["isCheck"]=eval("check"+key)
//   })
//   setVisible(false)
// }
// function OnBackOverlay(){
//   setVisible(false)
//   initCheck()

// }
            {/* <Overlay  isVisible={visible} windowBackgroundColor="rgba(108, 122, 137, 0.5)"
                        overlayBackgroundColor="white"
                        width="80%"
                        height="auto"
                        // overlayStyle={{justifyContent:"center"}}
                        onRequestClose={()=>{}}
                        onBackdropPress={() => {OnBackOverlay()}}> 
                        <View>
                            <Title h4 h4Style={{alignSelf:"center"}}>הגבלות</Title>
                            <View>
                                {
                                  Object.keys(limits).map((key,index)=>{
                                    return(
                                      <View style={{flexDirection:"row",alignSelf:"flex-start"}}>
                                            <CheckBox  containerStyle={{backgroundColor:"white",borderColor:"white",alignSelf:"center"}} title={limits[key]['name']} textStyle={{fontSize:20,textAlignVertical:"center"}} onPress={()=>eval("setCheck"+key)(!eval("check"+key))} checked={eval("check"+key)}/>
                                      </View>

                                    )
                                  })
                                }
                            </View>
                            <Button title={"אישור"} onPress={()=>onAcceptLimits()}/>
                        </View>
            </Overlay> */}
           {/* <TouchableOpacity>
            <View style={{backgroundColor:"#8fbc8f",borderRadius:10,marginTop:10}}>
                  <Title h3 h3Style={{alignSelf:"center"}}>{props.userDetails.name}</Title>
                  <Title h4 h4Style={styles.subTitle}>מחלקה {props.userDetails.unit}</Title>
            </View>
           </TouchableOpacity> */}

               // <View key={key} style={{borderColor:"white",justifyContent:"center",shadowColor:"#000",
    //             shadowOffset:{width:0,height:2} , shadowRadius:0.25,elevation:3
    //             ,borderWidth:1,backgroundColor:"white",width:height*0.12,height:height*0.12,borderRadius:height*0.12*0.5}}>
    //     <Icon size={50} name={lastExamIcon[key]['units']} type={lastExamIcon[key]['type']} color={lastExamIcon[key]['color']}/>
    //     <Text style={{alignSelf:"center"}}> {lastExam[key]['name']} {lastExam[key]['grade']}</Text>
    // </View>