import React, { useState, useEffect } from "react";
import SearchHeader from "../Components/SearchHeader";
import PlatoonList from "../Components/PlatoonList";
import AddUserModal from "../Components/AddUserModal";
import { Icon } from "react-native-elements";
import { ScrollView } from "react-native";
import axios from "axios";
import {
  StyleSheet,
  Text,
  Keyboard,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import {
  NavigationScreenProp,
  NavigationParams,
  NavigationState,
} from "react-navigation";

// var platoons: {[id:number] : platoon} = {1:{name:"1",company_id:1},2:{name:"2",company_id:1},3:{name:"3",company_id:1}};
// var participantsDB: {[id:number]:ParticipantsProps}={1:{name:"כהן דוד",platoon:1},6:{name:"כהן דttוד",platoon:1},2:{name:"אורי טרבלסי",platoon:2},3:{name:"אורי טרבלסי",platoon:3}}
// I18nManager.forceRTL(true);
// var platoonHeight=Object.keys(platoons).length
interface Iprops {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}
interface IParticipants {
  [participantId: number]: { name: string; platoon_id: number };
}

interface platoon {
  name: string;
}
interface Iplatoon {
  id: number;
  name: string;
}
interface IplatoonParticipantsId {
  [id: number]: number[];
}

const Participants: React.FC<Iprops> = (props: Iprops) => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [isAddUserModalVisible, setIsAddUserModalVisible] = useState<boolean>(
    false
  );
  // const [searchParticIdRes,setSearchParticIdRes]=useState<number[]>(Object.keys(participantsDB).map(x=>parseInt(x)))
  // const [participants,setParticpants]=useState <{[id:number]:ParticipantsProps}>(participantsDB)
  const [participants, setParticpants] = useState<IParticipants>({});
  const [platoons, setPlatoons] = useState<Iplatoon>();
  const [searchParticIdRes, setSearchParticIdRes] = useState<number[]>([]);

  useEffect(() => {
    const getDataFromDB = async () => {
      const participantsQuery = "http://192.168.43.224:5001/api/persons/getAll";
      const participantsRes = await axios
        .get(participantsQuery)
        .then((res) => {
          return res.data.data;
        })
        .catch((err) => console.log(err));
      setParticpants(participantsRes);
      setSearchParticIdRes(
        Object.keys(participantsRes).map((x) => parseInt(x))
      );
      const platoonsQuery = "http://192.168.43.224:5001/api/platoons/getAll";
      const platoonsRes = await axios
        .get(platoonsQuery)
        .then((res) => {
          return res.data.data;
        })
        .catch((err) => console.log(err));
      setPlatoons(platoonsRes);
      // setIsLoading(false);
    };
    getDataFromDB();
  }, []);
  useEffect(() => {
    if (participants && searchParticIdRes && platoons) setIsLoading(false);
    else setIsLoading(true);
  }, [participants, searchParticIdRes, platoons]);
  // var platoonHeight = Object.keys(platoons).length;
  /*
    insert to DB 
    get from dB
    setSearchParticIdRes
    setPaerticipants
  */
  const AddUser: Function = (platoonId: number, particName: string) => {
    let idArray = Object.keys(participants).map((participantId) => {
      return Number(participantId);
    });
    let participantId = Math.max(...idArray) + 1;
    participants[participantId] = { name: particName, platoon_id: platoonId };
    setSearchParticIdRes(
      Object.keys({ ...participants }).map((x) => parseInt(x))
    );
    setParticpants({ ...participants });
  };

  const searchResultToSearchPerPlatoon: Function = (
    searchParticIdRes: number[]
  ) => {
    let platoonParticipantsId: IplatoonParticipantsId = {};
    if (searchParticIdRes.length > 0) {
      searchParticIdRes.map((participantId) => {
        let platoonId = participants[participantId]["platoon_id"];
        if (platoonParticipantsId[platoonId]) {
          platoonParticipantsId[platoonId] = [
            ...platoonParticipantsId[platoonId],
            participantId,
          ];
        } else {
          platoonParticipantsId[platoonId] = [participantId];
        }
      });
    } else {
      platoonParticipantsId = {};
    }
    return platoonParticipantsId;
  };
  let platoonParticipantsId: IplatoonParticipantsId = searchResultToSearchPerPlatoon(
    searchParticIdRes
  );

  return isLoading ? (
    <Text>Loading...</Text>
  ) : (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        {platoons ? (
          <AddUserModal
            isVisble={isAddUserModalVisible}
            setVisible={setIsAddUserModalVisible}
            platoonNum={platoons}
            AddUser={AddUser}
          />
        ) : null}
        <View style={{ flex: 1 }}>
          <View style={styles.searchRowContainer}>
            <SearchHeader
              participants={participants}
              updatSearch={setSearchParticIdRes}
              style={styles.SearchHeaderStyle}
            />
            <View style={styles.addUser}>
              <Icon
                size={25}
                name="add-user"
                type="entypo"
                onPress={() => setIsAddUserModalVisible(true)}
              />
            </View>
          </View>
        </View>

        <View style={{ flex: 9 }}>
          <ScrollView>
            {Object.keys(platoonParticipantsId).map((key, index) => {
              let platoonParticipants: IParticipants = {};
              platoonParticipantsId[parseInt(key)].map(
                (participantId, index) => {
                  platoonParticipants[participantId] =
                    participants[participantId];
                }
              );
              return (
                <PlatoonList
                  navigation={props.navigation}
                  platoonName={key}
                  platoonParticipants={platoonParticipants}
                  key={key}
                />
              );
            })}
          </ScrollView>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default Participants;
const styles = StyleSheet.create({
  container: {
    // paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
    flex: 1,
  },
  searchRowContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#1e90ff",
  },
  addUser: {
    flex: 1,
    alignSelf: "center",
    paddingRight: 5,
  },
  SearchHeaderStyle: {
    flex: 7,
    alignSelf: "center",
    padding: 8,
  },
});
