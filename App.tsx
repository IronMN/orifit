import React from 'react';
import { StyleSheet, Text, View ,Platform,StatusBar} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack'
import {createAppContainer} from 'react-navigation'
import Participants from './Screens/Participants'
import UserScreen from './Screens/UserScreen'
// import {ParticipantsProps,unit} from './Interfaces/ParticipantsProps' 

// var participantsDB: {[id:number]:ParticipantsProps}={1:{name:"כהן דוד",unit:1},6:{name:"כהן דttוד",unit:1},2:{name:"אורי טרבלסי",unit:2},3:{name:"אורי טרבלסי",unit:3}}

const Navi=createAppContainer(createStackNavigator(
  {
    participantes:Participants,
    user:UserScreen
  },
  {
    defaultNavigationOptions:{
      headerShown:false
    }
  }
))
export default function App() {
  return (
    <View style={styles.container}>
        <Navi/>
    </View>
    // <UserScreen userDetails={participantsDB[1]}/>
      );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
    flex: 1,
  },
});
