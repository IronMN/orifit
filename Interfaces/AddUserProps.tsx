interface Platoon{
    name:string
  }
export interface AddUserProps{
    isVisble:boolean,
    setVisible:React.Dispatch<React.SetStateAction<boolean>>,
    platoonNum: {[id:number] : Platoon}
    AddUser:Function

  }