export interface CircleProps{
    color:string,
    grade:string|number
    IconType:string,
    units:string
    name:string
}