export interface IParticipants{
    name:string,
    platoon_id:number
  }
export interface UnitListProps{
    unitName:string,
    key:string,
    unitParticipants:{[id:number]:IParticipants},
    style?:any,
    navigation?:any
}

