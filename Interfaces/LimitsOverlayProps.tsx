export interface LimitsOverlayProps{
    visible:boolean,
    limits:{[id:number]:{name:string,isCheck:boolean}},
    setVisible:(value: React.SetStateAction<boolean>) => void
}



