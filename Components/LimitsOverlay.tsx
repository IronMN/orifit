import React,{useState} from 'react';
import { View ,StatusBar,Dimensions } from 'react-native';
import {Text as Title,Overlay ,Button,CheckBox } from 'react-native-elements'
import {LimitsOverlayProps} from '../Interfaces/LimitsOverlayProps'
var {height}=Dimensions.get("window")
const StatusBarHeight:number|undefined=StatusBar.currentHeight
if (StatusBarHeight){
    height=height-StatusBarHeight
}

 const LimitsOverlay:React.FC<LimitsOverlayProps>=(props)=> {
     const [check1, setCheck1] = useState(props.limits[1]["isCheck"]);
     const [check2, setCheck2] = useState(props.limits[2]["isCheck"]);
     const [check3, setCheck3] = useState(props.limits[3]["isCheck"]);
    function initCheck(){
        Object.keys(props.limits).map((key1,index)=>{
            var key=parseInt(key1)
        eval("setCheck"+key)(props.limits[key]["isCheck"])
        })
    }
    function onAcceptLimits(){
        Object.keys(props.limits).map((key1,index)=>{
           var key=parseInt(key1)
        props.limits[key]["isCheck"]=eval("check"+key)
        })
        props.setVisible(false)
    }
    function OnBackOverlay(){
        props.setVisible(false)
        initCheck()
    
    }
    return (
            <Overlay  isVisible={props.visible}
            // overlayStyle={{justifyContent:"center"}}
            onRequestClose={()=>{}}
            onBackdropPress={() => {OnBackOverlay()}}> 
                <View>
                    <Title h4 h4Style={{alignSelf:"center"}}>הערות</Title>
                    <View>
                        {
                        Object.keys(props.limits).map((key1,index)=>{
                            var key=parseInt(key1)
                            return(
                            <View key={key} style={{flexDirection:"row",alignSelf:"flex-start"}}>
                                    <CheckBox  containerStyle={{backgroundColor:"white",borderColor:"white",alignSelf:"center"}} title={props.limits[key]['name']} textStyle={{fontSize:20,textAlignVertical:"center"}} onPress={()=>eval("setCheck"+key)(!eval("check"+key))} checked={eval("check"+key)}/>
                            </View>
                            )})
                        }
                    </View>
                    <Button title={"אישור"} onPress={()=>onAcceptLimits()}/>
                </View>
            </Overlay>
             )
    }
export default LimitsOverlay