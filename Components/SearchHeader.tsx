import React,{useState} from 'react';
import {  View , StyleSheet} from 'react-native';
import  {SearchBar}  from 'react-native-elements';
import {SearchProps} from '../Interfaces/SearchProps'

function SearchNameInDict(text:string,dict: {[id:number] : any}){
    text = text.trim().toLowerCase();
    let data = Object.keys(dict).filter((l,index) => {
      let l2=parseInt(l)
      return (dict[l2]["name"].toLowerCase().match(text))});
    return data.map(x=>parseInt(x))
  } 

const SearchHeader:React.FC<SearchProps>=(props)=> {
    const [searchValue,setSearchValue]=useState<string>("")
    const OnSearch:Function=(text:string)=>{
            setSearchValue(text)
            // var newUnits: {[id:number] : number[]} = {};
            let searchRes=SearchNameInDict(text,props.participants)
            props.updatSearch(searchRes)
          }
    return (
      <View style={props.style}>
      <SearchBar  
        inputContainerStyle={style.searchInputContainer}    
        containerStyle={style.searchContainer}
        value={searchValue} placeholder="חיפוש"
        onChangeText={(e)=>OnSearch(e)} />
      </View>
    )
}
export default SearchHeader
const style=StyleSheet.create({
  searchContainer:{
    borderBottomColor:"#1e90ff",
    borderTopColor:"#1e90ff",
    backgroundColor: 'white',
    padding: 1 ,
    borderRadius:15
  },
  searchInputContainer:{
    backgroundColor: 'white',
    borderRadius:15

  }
})
