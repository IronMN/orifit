import React from 'react';
import { StyleSheet , View} from 'react-native';
import { Text as Title } from 'react-native-elements'
import { LinearGradient } from 'expo-linear-gradient';
import {UserTitleProps} from '../Interfaces/UserTitleProps'

const UserTitle:React.FC<UserTitleProps>=(props)=> {
    return(
        <LinearGradient
        colors={ ['#6495ed', '#87cefa']}
        start= {{ x: 1, y: 0} }
        end= {{ x: 0.2, y: 0 }} 
        style={{ marginTop:10,alignItems: 'center', borderRadius: 5 }}>
        <View>
            <Title h3 h3Style={{alignSelf:"center"}}>{props.name}</Title>
            {console.log(props.unitName)}
            <Title h4 h4Style={styles.subTitle}>מחלקה {props.unitName}</Title>
        </View>
      </LinearGradient>
    )
}
export default UserTitle
const styles=StyleSheet.create({
    subTitle:{
        alignSelf:"center",
        fontWeight:"normal",
        fontSize:20,
        paddingBottom:5
      }
})