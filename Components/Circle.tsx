import React from 'react';
import { StyleSheet, Text, View ,Dimensions } from 'react-native';
import {Icon} from 'react-native-elements'
import {CircleProps} from '../Interfaces/CircleProps'


var {height,width}=Dimensions.get("window")
 const Circle:React.FC<CircleProps>=(props)=> {
    return (
        <View style={styles.container}>
            <Icon size={50} name={props.name} type={props.IconType} color={props.color}/>
            <Text style={{alignSelf:"center"}}> {props.units} {props.grade}</Text>
        </View>
            )
}

export default Circle

const styles = StyleSheet.create({
    container: {
        // paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
        borderColor:"white",
        justifyContent:"center",
        shadowColor:"#000",
        shadowOffset:{width:0,height:2},
        shadowRadius:0.25,
        elevation:3,
        borderWidth:1,
        backgroundColor:"white",
        width:height*0.12,
        height:height*0.12,
        borderRadius:height*0.12*0.5
    },

})

