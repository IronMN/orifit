import React,{useState} from 'react';
import {View,Text,Picker,StyleSheet} from 'react-native';
import { Input  } from 'react-native-elements';
import { Overlay,Button } from 'react-native-elements';
// import {AddUserProps} from "../Interfaces/AddUserProps"
import axios from 'axios'
// interface platoon{
//     name:string
//   }
// interface Props{
//     isVisble:boolean,
//     setVisible:React.Dispatch<React.SetStateAction<boolean>>,
//     platoonNum: {[id:number] : platoon}
//     AddUser:(platoonId: number, particName: string)=> void

//   }

interface Iplatoon{
    id: number;
    name: string;
  }
interface AddUserProps{
    isVisble:boolean,
    setVisible:React.Dispatch<React.SetStateAction<boolean>>,
    platoonNum: Iplatoon
    AddUser:Function

  }
const AddUserModal:React.FC<AddUserProps>=(props)=> {
    const [selectedValue, setSelectedValue] = useState<string>("choose");
    const [name,setName]=useState<string>("")
    const [errNameMsg,setErrNameMsg]=useState<string>("")
    const [errPlatoonChoose,setErrPlatoonChoose]=useState<string>("black")
    const onSumbit:Function=async()=>{
    // var isSubOk:Boolean=false
    if ((name.trim()!="") && (selectedValue!="choose")){
        props.setVisible(false)
        props.AddUser(parseInt(selectedValue),name)
        setName("")
        setSelectedValue("choose")
        const platoonsQuery="http://192.168.43.224:5001/api/persons/add";
        const platoonsRes=await axios.post(platoonsQuery,{name:name,platoon_id:parseInt(selectedValue)})
    }
    else{
        if (name.trim()=="")
        {
            setErrNameMsg("הכנס שם משתתף בבקשה")
        }
        else{setErrNameMsg("")}
        if (selectedValue=="choose")
        {
            setSelectedValue("choose")
            setErrPlatoonChoose("red")
        }
        else{setErrPlatoonChoose("black")}

    }
    }
    return(
        <View>
            {props.isVisble?
            <Overlay  isVisible={props.isVisble} 
                        overlayStyle={{justifyContent:"center",height:"auto",width:"80%"}}
                        onRequestClose={()=>{}}
                        onBackdropPress={() =>
                         { props.setVisible(false)
                         setErrNameMsg("")
                         setErrPlatoonChoose("")
                         setSelectedValue("choose")
                         setName("")}}>
                <View>
                    <View style={styles.addUserTitleConrainer}>
                        <Text style={styles.addUsertitle} >הוספת משתמש</Text>
                    </View>
                    <View>
                        <View> 
                            <View style={styles.addFullName}>
                                <Input value={name} label="שם מלא" errorMessage ={errNameMsg}  onChangeText={(text)=>setName(text)} />
                            </View>
                            <View>
                                <Picker mode="dropdown" selectedValue={selectedValue} 
                                    onValueChange={(itemValue, itemIndex) =>setSelectedValue(itemValue)}>
                                    {
                                    Object.keys(props.platoonNum).map((key,index)=>{return (
                                    <Picker.Item  key={index}  label={"מחלקה "+(index+1)} value={index+1}/>)})
                                    }
                                    <Picker.Item label="בחר מחלקה"   value="choose" color={errPlatoonChoose} />
                                </Picker>
                            </View>
                            <Button  title='אישור' onPress={()=>onSumbit()}  />
                        </View>
                    </View>
                </View>
            </Overlay>:null}
        </View>
    )
    

}
const styles = StyleSheet.create({
addUserTitleConrainer:{
    backgroundColor:"grey",
    opacity:0.3,
    marginBottom:10
},
addUsertitle:{
    fontSize:20,
    color:"black"
},
addFullName:{ 
    marginBottom:10
}
  
  });
export default AddUserModal
