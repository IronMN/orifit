import React,{useState} from 'react';
import { StyleSheet, Text , View,Dimensions, ScrollView } from 'react-native';
import { ListItem } from 'react-native-elements'
import  Icon from 'react-native-vector-icons/Octicons'
export interface IParticipants{
    name:string,
    platoon_id:number
  }
export interface PlatoonListProps{
    platoonName:string,
    key:string,
    platoonParticipants:{[id:number]:IParticipants},
    navigation?:any
}

const height:number=Dimensions.get('window').height
const PlatoonList:React.FC<PlatoonListProps>=(props)=> {
    return(
        <View style={styles.platoonListContainer} key={props.platoonName}>
            <View  style={styles.titleContainer}>
                <Text style={styles.platoonTitle}>מחלקה {props.platoonName}</Text>
            </View>
            <ScrollView nestedScrollEnabled = {true} >
                {Object.keys(props.platoonParticipants).map((key1,index)=>{
                var key=parseInt(key1)
                return(
                    <ListItem titleStyle={{fontSize:18}} title={props.platoonParticipants[key]["name"]} key={index} 
                                style={styles.participantContainer} rightElement={
                                <Icon name="primitive-dot" size={20} color={"green"} style={{alignSelf:"flex-end",opacity:0.8}} />}
                                onPress={() => props.navigation.navigate('user',{name:props.platoonParticipants[key]["name"],platoon:props.platoonParticipants[key]["platoon_id"]})}/>)})}
            </ScrollView>
        </View>
)

}

const styles=StyleSheet.create({
    participantContainer:{
        borderBottomColor: "rgba(220,220,220,0.5)",
        borderBottomWidth: 1,
         flexDirection:"row",
         alignItems:"center" 
    },
    titleContainer:{
        backgroundColor:"#1e90ff",
        borderRadius:5,
        opacity:0.7,
        padding:2
    },
    platoonTitle:{
        fontSize:20,
        alignSelf:"center",
        fontWeight:"bold"
    },
    platoonListContainer: {
        marginHorizontal: 30,
        paddingTop: 20,
        maxHeight:height*0.3*0.9,
      },
})
export default PlatoonList 