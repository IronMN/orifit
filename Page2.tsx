import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function Page2(props:any) {
  return (
    <View style={styles.container}>
      <Text>{props.navigation.navigate}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
